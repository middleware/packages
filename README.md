# Package repository

This project stores external packages that are not or cannot be found in central repositories like maven central.

See https://code.vt.edu/middleware/packages/-/packages

## YourKit Docker Binary

### Download the latest binary
Access a URL like https://www.yourkit.com/download/docker/YourKit-JavaProfiler-2022.9-docker.zip
There is no way to do a directory listing, try to match the version of your client.
The URL does not contain the minor version of the file (e.g. -b179), which means that future requests to this URL may return a different file.
It appears that matching the major version is all that is required to guarantee client support.

### Publish a new package
curl -v --header "PRIVATE-TOKEN: <your_access_token>" \
     --upload-file path/to/YourKit-JavaProfiler-2022.9-b179-docker.zip \
     "https://code.vt.edu/api/v4/projects/15567/packages/generic/YourKit-JavaProfiler-Docker/2022.9-b179/YourKit-JavaProfiler-2022.9-b179-docker.zip"

The -b179 should be included in the package version.
Access token must have `api` permissions.
Once uploaded the package download URL can be used in automated processes, it will be of the form https://code.vt.edu/middleware/packages/-/package_files/6954/download

